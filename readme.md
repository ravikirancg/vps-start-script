1. Swap: 

```
wget https://gitlab.com/ravikirancg/vps-start-script/-/raw/8428c19f400afd4a5854b17b120653649a05e091/swap.sh
chmod +x swap.sh
sudo /bin/bash swap.sh
```

2. Oracle Linux Open all ports
```
sudo su
iptables-save > /home/ubuntu/IPtablesbackup.txt
sudo iptables -L INPUT -v -n --line-numbers
sudo iptables -D INPUT 6
iptables -I INPUT -j ACCEPT
iptables-save  > /etc/iptables/rules.v4

```
3. Oracle Linux Open caprover ports
```
sudo su
iptables-save > /home/ubuntu/IPtablesbackup.txt
iptables -I INPUT -p tcp --dport 80,443,3000,996,7946,4789,2377 -j ACCEPT
iptables -I INPUT -p udp --dport 7946,4789,2377 -j ACCEPT
iptables-save  > /etc/iptables/rules.v4

```
4. Docker

```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $USER

```
5. Install caprover
```
docker run -p 80:80 -p 443:443 -p 3000:3000 -v /var/run/docker.sock:/var/run/docker.sock -v /captain:/captain caprover/caprover
```
6. Swarm
```
Master: docker swarm join-token worker
Worker:  docker swarm join --token SWMTKN-1-06ysfvmr00tsohemk7oie996xtu48aywwxne6f72pv515vecpj-8glrib9dgplmnmebyk9wevfxp 150.136.136.232:2377 --advertise-addr 150.136.248.15
```
